use num::{Complex, complex::ComplexFloat};
use crate::root_lists::{RANDOM_POINTS, SQUARE_GRID};

const LARGE: f64 = 1.0e40;

// p / dp for Chebyshev implemented recursively
pub fn cheb_recursive(n: u32, z: Complex<f64>)
-> (Complex<f64>, Complex<f64>, Complex<f64>, Complex<f64>) {
    if n == 0 {
        ((1.).into(), (0.).into(), (0.).into(), (0.).into())
    } else if n == 1 {
        (z, (1.).into(), (1.).into(), (0.).into())
    } else {
        let (tn1, tn2, dtn1, dtn2) = cheb_recursive(n-1, z);
        let c = 2.*z*tn1 - tn2;
        let dc = 2.*z*dtn1 + 2.*tn1 - dtn2;
        (c, tn1, dc, dtn1)
    }
}

pub fn cheb_recursive_opt(n: u32, z: Complex<f64>)
-> (Complex<f64>, Complex<f64>, Complex<f64>, Complex<f64>) {
    if n == 0 {
        ((1.).into(), (0.).into(), (0.).into(), (0.).into())
    } else if n == 1 {
        (z, (1.).into(), (1.).into(), (0.).into())
    } else {
        let (tn1, tn2, dtn1, dtn2) = cheb_recursive(n-1, z);
        if tn1.abs() > LARGE && dtn1.abs() > LARGE {
            //println!("got used");
            let tn1 = tn1 / LARGE;
            let tn2 = tn2 / LARGE;
            let dtn1 = dtn1 / LARGE;
            let dtn2 = dtn2 / LARGE;

            let c = 2.*z*tn1 - tn2;
            let dc = 2.*z*dtn1 + 2.*tn1 - dtn2;
            (c, tn1, dc, dtn1)
        } else {
            let c = 2.*z*tn1 - tn2;
            let dc = 2.*z*dtn1 + 2.*tn1 - dtn2;
            (c, tn1, dc, dtn1)
        }
    }
}

pub fn mdlbrt_hyp_cmp(n: u32, c: Complex<f64>) -> Complex<f64> {
    if n == 1 {
        c
    } else {
        mdlbrt_hyp_cmp(n-1, c).powu(2) + c
    }
}


pub fn dmdlbrt_hyp_cmp(n: u32, c: Complex<f64>) -> Complex<f64> {
    if n == 1 {
        (1.).into()
    } else {
        1. + 2.*mdlbrt_hyp_cmp(n-1, c)*dmdlbrt_hyp_cmp(n-1, c)
    }
}

pub fn mdlbrt_hyp_cmp_general(n: u32, c: Complex<f64>) -> (Complex<f64>, Complex<f64>) {
    if n == 1 {
        (c, (1.).into())
    } else {
        let (m, dm) = mdlbrt_hyp_cmp_general(n-1, c);
        (m.powu(2) + c, 1. + 2.*m*dm)
    }
}


fn start_mdlbrt(c: Complex<f64>) -> Complex<f64> { c*c + c }
fn start_dmdlbrt(c: Complex<f64>) -> Complex<f64> { 1. + 2.*c }

fn step_mdlbrt(c: Complex<f64>, m: Complex<f64>, large: bool) -> Complex<f64> {
    if large { m*m }
    else { m*m + c }
}
fn step_dmdlbrt(m: Complex<f64>, dm: Complex<f64>, large: bool) -> Complex<f64> {
    if large { 2.*m*dm }
    else { 1. + 2.*m*dm }
}

pub fn mdlbrt_hyp_cmp_opt(n: u32, c: Complex<f64>) -> Complex<f64> {
    let mut m = start_mdlbrt(c);
    let mut dm = start_dmdlbrt(c);

    let mut large = false;

    for _ in 2..n {
        dm = step_dmdlbrt(m, dm, large);
        m = step_mdlbrt(c, m, large);

        if m.abs() > LARGE && dm.abs() > LARGE {
            large = true;

            m /= LARGE;
            dm /= LARGE;
        }

        //dbg!(apply_large);
        /* if (m / dm).abs() < 1.0e-15 {
            dbg!(m);
            dbg!(dm);
            dbg!(c);
            dbg!(mdlbrt_hyp_cmp_general(10, c));
        } */
    }
    m / dm
}


const I: Complex<f64> = Complex { re: 0., im: 1. };

fn start_iterated_quadratic(z: Complex<f64>) -> Complex<f64> { z*z + I }
fn start_diterated_quadratic(z: Complex<f64>) -> Complex<f64> { 2.*z }

fn step_iterated_quadratic(z: Complex<f64>, large: bool) -> Complex<f64> {
    if large { z*z }
    else { z*z + I }
}
fn step_diterated_quadratic(i: Complex<f64>, di: Complex<f64>, large: bool) -> Complex<f64> {
    2.*i*di
}

fn end_iterated_quadratic(i: Complex<f64>, z: Complex<f64>, large: bool) -> Complex<f64> {
    if large { i }
    else { i-z }
}
fn end_diterated_quadratic(di: Complex<f64>, large: bool) -> Complex<f64> {
    if large { di }
    else { di-1. }
}

pub fn iterated_quadratic_opt(n: u32, z: Complex<f64>) -> Complex<f64> {
    let mut i = start_iterated_quadratic(z);
    let mut di = start_diterated_quadratic(z);

    let mut large = false;

    for _ in 2..n {
        di = step_diterated_quadratic(i, di, large);
        i = step_iterated_quadratic(i, large);

        if i.abs() > LARGE && di.abs() > LARGE {
            large = true;

            i /= LARGE;
            di /= LARGE;
        }
    }
    di = end_diterated_quadratic(di, large);
    i = end_iterated_quadratic(i, z, large);

    i / di
}

pub fn cheb_normal(z: Complex<f64>) -> Complex<f64> {
    1.
    - 200.*z.powu(2)
    + 6600.*z.powu(4)
    - 84480.*z.powu(6)
    + 549120.*z.powu(8)
    - 2050048.*z.powu(10)
    + 4659200.*z.powu(12)
    - 6553600.*z.powu(14)
    + 5570560.*z.powu(16)
    - 2621440.*z.powu(18)
    + 524288.*z.powu(20)
}

pub fn dcheb_normal(z: Complex<f64>) -> Complex<f64> {
    10485760.*z.powu(19)
    - 47185920.*z.powu(17)
    + 89128960.*z.powu(15)
    - 91750400.*z.powu(13)
    + 55910400.*z.powu(11)
    - 20500480.*z.powu(9)
    + 4392960.*z.powu(7)
    - 506880.*z.powu(5)
    + 26400.*z.powu(3)
    - 400.*z.powu(1)
}

pub fn cheb_ruffini_eval(z: Complex<f64>) -> Complex<f64> {
    let coeffs = vec![
        (524288.).into(),
        (0.).into(),
        (-2621440.).into(),
        (0.).into(),
        (5570560.).into(),
        (0.).into(),
        (-6553600.).into(),
        (0.).into(),
        (4659200.).into(),
        (0.).into(),
        (-2050048.).into(),
        (0.).into(),
        (549120.).into(),
        (0.).into(),
        (-84480.).into(),
        (0.).into(),
        (6600.).into(),
        (0.).into(),
        (-200.).into(),
        (0.).into(),
        (1.).into(),
    ];
    let deg = 20;
    if z.abs() < 1.0 {
        // Common Ruffini-Horner evaluation
        let mut quotient_coeffs = vec![coeffs[0]];
        for (i, coeff) in coeffs.iter().skip(1).enumerate() {
            quotient_coeffs.push(quotient_coeffs[i] * z + coeff);
        }
        // The remainder is the polynomial evaluated at z
        let p = quotient_coeffs.pop().unwrap();

        // The carry-over computes the derivative at z
        let mut carry_over = quotient_coeffs[0];
        for coeff in quotient_coeffs.iter().skip(1) {
            carry_over = carry_over * z + coeff;
        }

        p / carry_over
    } else {
        // Special evaluation taken from dario bini's paper from 96
        let y = z.inv();
        let mut quotient_coeffs = vec![*coeffs.last().unwrap()];
        for (i, coeff) in coeffs.iter().rev().collect::<Vec<&Complex<f64>>>().iter().skip(1).enumerate() {
            quotient_coeffs.push(quotient_coeffs[i] * y + *coeff);
        }
        let p = quotient_coeffs.pop().unwrap();

        // The carry-over computes the derivative at z
        let mut carry_over = quotient_coeffs[0];
        for coeff in quotient_coeffs.iter().skip(1) {
            carry_over = carry_over * y + coeff;
        }

        (deg as f64 * y - y*y * carry_over / p).inv()
    }
}

pub fn laguerre(k: u32, z: Complex<f64>) -> Complex<f64> {
    if k == 0 {
        (1.).into()
    } else if k == 1 {
        1. - z
    } else {
        ((2.*(k-1) as f64 + 1. - z) * laguerre(k-1, z) - (k-1) as f64 * laguerre(k-2, z)) / k as f64
    }
} 

pub fn dlaguerre(k: u32, z: Complex<f64>) -> Complex<f64> {
    if k == 0 {
        (0.).into()
    } else if k == 1 {
        (-1.).into()
    } else {
        (dlaguerre(k-1, z)) - (laguerre(k-1, z))
    }
}

pub fn laguerre_general(k: u32, z: Complex<f64>) -> (Complex<f64>, Complex<f64>, Complex<f64>) {
    if k == 1 {
        (1. - z, (1.).into(), (-1.).into())
    } else {
        let (l1, l2, dl1) = laguerre_general(k-1, z);
        (((2.*(k-1) as f64 + 1. - z) * l1 - (k-1) as f64 * l2) / k as f64, l1, dl1 - l1)
    }
}

pub fn laguerre_general_opt(k: u32, z: Complex<f64>) -> (Complex<f64>, Complex<f64>, Complex<f64>) {
    if k == 1 {
        (1. - z, (1.).into(), (-1.).into())
    } else {
        let (l1, l2, dl1) = laguerre_general_opt(k-1, z);
        if l1.abs() > LARGE && dl1.abs() > LARGE {
            //println!("got used");
            let l1 = l1 / LARGE;
            let l2 = l2 / LARGE;
            let dl1 = dl1 / LARGE;
            (((2.*(k-1) as f64 + 1. - z) * l1 - (k-1) as f64 * l2) / k as f64, l1, dl1 - l1)
        } else {
            (((2.*(k-1) as f64 + 1. - z) * l1 - (k-1) as f64 * l2) / k as f64, l1, dl1 - l1)
        }
    }
}

pub fn legendre(k: u32, z: Complex<f64>) -> Complex<f64> {
    if k == 0 {
        (1.).into()
    } else if k == 1 {
        z
    } else {
        ((2*k-1) as f64 * z * legendre(k-1, z) - (k-1) as f64 * legendre(k-2, z)) / k as f64
    }
}
    
pub fn dlegendre(k: u32, z: Complex<f64>) -> Complex<f64> {
    if k == 0 {
        (0.).into()
    } else if k == 1 {
        (1.).into()
    } else {
        k as f64 * legendre(k-1, z) + z * dlegendre(k-1, z)
    }
}

pub fn legendre_general(k: u32, z: Complex<f64>) -> (Complex<f64>, Complex<f64>, Complex<f64>) {
    if k == 1 {
        (z, (1.).into(), (1.).into())
    } else {
        let (l1, l2, dl1) = legendre_general(k-1, z);
        (((2*k-1) as f64 * z * l1 - (k-1) as f64 * l2) / k as f64, l1, k as f64 * l1 + z * dl1)
    }
}

pub fn hermite(n: u32, z: Complex<f64>) -> Complex<f64> {
    if n == 1 {
        2.*z
    } else {
        2.*z*hermite(n-1, z) - dhermite(n-1, z)
    }
}

pub fn dhermite(n: u32, z: Complex<f64>) -> Complex<f64> {
    if n == 1 {
        (2.).into()
    } else {
        2. * n as f64 * hermite(n-1, z)
    }
}

pub fn hermite_general(n: u32, z: Complex<f64>) -> (Complex<f64>, Complex<f64>) {
    if n == 1 {
        (2.*z, (2.).into())
    } else {
        let (h, dh) = hermite_general(n-1, z);
       /*  if (h / dh).is_nan() {
            dbg!(h / dh);
            dbg!(h);
            dbg!(dh);
            dbg!(n);
            //panic!();
        } */
        (2. * z * h - dh, 2. * n as f64 * h)
    }
}

fn hermite_opt_proto(n: u32, z: Complex<f64>) -> (Complex<f64>, Complex<f64>) {
    if n == 1 {
        (2.*z, (1.).into())
    } else {
        let (h1, h2) = hermite_opt_proto(n-1, z);
        if h1.abs() > LARGE && h2.abs() > LARGE {
            let h1 = h1 / LARGE;
            let h2 = h2 / LARGE;
            (2. * (z * h1 - (n-1) as f64 * h2), h1)
        } else {
            (2. * (z * h1 - (n-1) as f64 * h2), h1)
        }
    } 
}

pub fn hermite_opt(n: u32, z: Complex<f64>) -> Complex<f64> {
    let (h1, h2) = hermite_opt_proto(n-1, z);
    let frac = h2 / h1;
    z / (n as f64) - ((n - 1) as f64 / n as f64) * frac
}

pub fn wilkinson(n: u32, z: Complex<f64>) -> Complex<f64> {
    if n == 1 {
        z - 1.
    } else {
        (z - n as f64) * wilkinson(n-1, z)
    }
}

pub fn dwilkinson(n: u32, z: Complex<f64>) -> Complex<f64> {
    if n == 1 {
        (1.).into()
    } else {
        wilkinson(n-1, z) + (z - n as f64) * dwilkinson(n-1, z)
    }
}

pub fn wilkinson_general(n: u32, z: Complex<f64>) -> (Complex<f64>, Complex<f64>) {
    if n == 1 {
        (z - 1., (1.).into())
    } else {
        let (w, dw) = wilkinson_general(n-1, z);
        ((z - n as f64) * w, w + (z - n as f64) * dw)
    }
}

pub fn nroots(n: u32, z: Complex<f64>) -> Complex<f64> {
    z.powu(n) - 1.
}

pub fn dnroots(n: u32, z: Complex<f64>) -> Complex<f64> {
    n as f64 * z.powu(n-1)
}

pub fn nrooti(n: u32, z: Complex<f64>) -> Complex<f64> {
    z.powu(n) - I
}

pub fn dnrooti(n: u32, z: Complex<f64>) -> Complex<f64> {
    n as f64 * z.powu(n-1)
}

pub fn square_poly(n: i32, z: Complex<f64>) -> Complex<f64> {
    let mut result = (1.).into();
    for i in (-n/2)..=(n/2) {
        for j in (-n/2)..=(n/2) {
            result *= z - Complex { re: i as f64, im: j as f64 };
        }
    }
    result
}

pub fn dsquare_poly(n: i32, z: Complex<f64>) -> Complex<f64> {
    let mut result = (0.).into();
    let mut temporary_result: Complex<f64> = (1.).into();

    for a in (-n/2)..=(n/2) {
        for b in (-n/2)..=(n/2) {
            for i in (-n/2)..=(n/2) {
                for j in (-n/2)..=(n/2) {
                    if !(a == i && b == j) {
                        temporary_result *= z - Complex { re: i as f64, im: j as f64 };

                    }
                }
            }
            result += temporary_result;
            temporary_result = (1.).into();
        }
    }
    result
}


pub fn rand_on_disk_general(n: u32, z: Complex<f64>) -> (Complex<f64>, Complex<f64>) {
    if n == 1 {
        (z - RANDOM_POINTS[n as usize - 1], (1.).into())
    } else {
        let (r, dr) = rand_on_disk_general(n-1, z);
        ((z - RANDOM_POINTS[n as usize - 1]) * r, r + (z - RANDOM_POINTS[n as usize - 1]) * dr)
    }
}


pub fn square_grid_general(n: u32, z: Complex<f64>) -> (Complex<f64>, Complex<f64>) {
    if n == 1 {
        (z - SQUARE_GRID[n as usize - 1], (1.).into())
    } else {
        let (r, dr) = square_grid_general(n-1, z);
        if r.abs() > LARGE && dr.abs() > LARGE {
            let r = r / LARGE;
            let dr = dr / LARGE;
            ((z - SQUARE_GRID[n as usize - 1]) * r, r + (z - SQUARE_GRID[n as usize - 1]) * dr)
        } else {
            ((z - SQUARE_GRID[n as usize - 1]) * r, r + (z - SQUARE_GRID[n as usize - 1]) * dr)
        }
    }
}