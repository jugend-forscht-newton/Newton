use std::f64::consts;
use num::Complex;
use num::complex::ComplexFloat;
use std::fs::OpenOptions;
use std::io::prelude::*;

pub fn newton_output(
    start_r: f64, start_n0_cap: u32,
    eps_stop: f64, eps_root: f64,
    rfn: f64, deg: u32,
    pdp: &impl Fn(Complex<f64>) -> Complex<f64>
) -> std::io::Result<Vec<(Complex<f64>, u32)>> {
    // Set up file to write to
    // For some reason `truncate` has to be in this first line
    let _f = OpenOptions::new().create(true).write(true).truncate(true).open("orbits.txt")?;
    let mut file = OpenOptions::new().append(true).open("orbits.txt")?;

    // Set maximum number of iterations
    let max_iterations = 10 * deg;

    // Calculate N_0 with `start_n0_cap` and deg.
    let n0 = if deg * 4 > start_n0_cap { start_n0_cap } else { deg * 4 };

    // For keeping track of how many orbits have ever been created
    let mut ni = n0;

    // Calculate maximum number of possible refinement generations for each orbit
    // meaning how many new orbits it can produce in total.
    // Generations each orbit definitely does.
    let max_rfn_gen = (4.*deg as f64 / n0 as f64).log2().floor() as u32;    
    // How many orbits, after basic refinement generation is done, are additionally
    // allowed to create more orbits.
    let mut add_rfn_gen = deg * 4 - n0 * 2_u32.pow(max_rfn_gen);

    // Vector for tracking number of refinements. All values are set to `max_rfn_gen+1`
    // because every
    let mut rfn_count: Vec<u32> = (1..=n0).map(|_| (max_rfn_gen+1).into()).collect();

    // Variable for switching modes from still refining to not anymore
    let mut still_refining = true;

    dbg!(max_rfn_gen);
    dbg!(add_rfn_gen);
    //dbg!(rfn_count.clone());

    // First, create all the starting orbits.
    let mut z0 = vec![];
    for i in 0..n0 {
        z0.push(Complex::from_polar(start_r, i as f64 / n0 as f64 * 2. * consts::PI));
    }

    // Vector for storing thresholds for each orbit
    let mut t_n: Vec<Complex<f64>> = get_t(&z0);

    // Vector for storing thresholds for each orbit the last time they refined
    let mut t_n0: Vec<Complex<f64>> = t_n.clone();

    // Vector that stores the actual refinement conditions
    let mut rfn_condition: Vec<bool>;

    // Vector for all orbits
    let mut z_n: Vec<Complex<f64>> = z0;

    // Vector for all pdp(z) for z in z_n
    let mut pdp_n: Vec<Complex<f64>>;

    // Vector keeping track of all converged orbits (orbit, iteration)
    let mut z_converged: Vec<(Complex<f64>, u32)> = vec![];

    // Temporary vectors getting constructed (if they have to) each iteration
    let mut z_temp: Vec<Complex<f64>>;
    let mut t_n0_temp: Vec<Complex<f64>>;
    let mut rfn_count_temp: Vec<u32>;

    // Variable keeping track of iterations
    let mut iteration: u32 = 0;
    while max_iterations > iteration && z_n.len() != 0 {
        // New iteration
        iteration += 1;
        // ---WRITE--- z_n
        file.write_fmt(format_args!("{:?}\n", complex_vector_to_tuples(&z_n)))?;

        // ---UPDATE--- p_n
        pdp_n = z_n.iter().map(|z| pdp(*z)).collect();

        /* for (i, z) in pdp_n.iter().enumerate() {
            if z.is_nan() {
                dbg!(z);
                panic!("i: {}", i);
            } else if z.is_infinite() {
                dbg!(z);
                panic!("i: {}", i);
            }
        } */

        // Make one newton iteration
        // ---UPDATE--- z_n
        z_n = z_n.iter().enumerate().map(|(i, z)| z - pdp_n[i]).collect();

        // BEGIN REFINEMENT CHECKS
        if still_refining {
            // Calculate thresholds
            // ---UPDATE--- t_n
            t_n = get_t(&z_n);

            //dbg!(t_n.clone());

            // Calculate refinement conditions
            // ---UPDATE--- rfn_condition
            rfn_condition = get_rfn_condition(&t_n, &t_n0, rfn);

            //dbg!(rfn_condition.clone());

            // Construct new vectors by resetting all the temporary vectors
            z_temp = vec![];
            t_n0_temp = vec![];
            rfn_count_temp = vec![];

            for (i, refine) in rfn_condition.iter().enumerate() {
                // Setup indices
                // i-1 and i+1 while accounting for end points of the vector
                let l = z_n.len() - 1;
                //let j = if i == 0 { l } else { i-1 }; // i-1
                let k = if i == l { 0 } else { i+1 }; // i+1
                //dbg!(i);
                //dbg!(j);
                //dbg!(k);

                // Refine as usual if needed and this orbit still is allowed to
                if *refine && rfn_count[i] > 1 {
                    // Insert old orbit at i
                    z_temp.push(z_n[i]);
                    t_n0_temp.push(t_n0[i]);
                    rfn_count_temp.push(rfn_count[i] - 1);

                    // Insert new orbit between i and i+1
                    ni += 1;
                    let new = (z_n[i]+z_n[k]) / 2.;
                    z_temp.push(new);
                    t_n0_temp.push(get_t_at_i(new, z_n[i], z_n[k]));
                    rfn_count_temp.push(rfn_count[i] - 1);
                } else if *refine && rfn_count[i] == 1 && add_rfn_gen != 0 {
                    // Refine specially

                    // Insert old orbit
                    z_temp.push(z_n[i]);
                    t_n0_temp.push(t_n0[i]);
                    rfn_count_temp.push(rfn_count[i] - 1);

                    // Insert new orbit
                    add_rfn_gen -= 1;
                    ni += 1;
                    let new = (z_n[i]+z_n[k]) / 2.;
                    z_temp.push(new);
                    t_n0_temp.push(get_t_at_i(new, z_n[i], z_n[k]));
                    rfn_count_temp.push(rfn_count[i] - 1);
                } else {
                    // Add orbit without refining
                    z_temp.push(z_n[i]);
                    t_n0_temp.push(t_n0[i]);
                    rfn_count_temp.push(rfn_count[i]);
                }
            }

            // Now, the new values for all vectors should have been constructed
            // and can be stored in z_n, t_n0, and rfn_count
            // ---UPDATE--- z_n
            z_n = z_temp;
            // ---UPDATE--- t_n0
            t_n0 = t_n0_temp;
            // ---UPDATE--- rfn_count
            rfn_count = rfn_count_temp;

            //dbg!(rfn_count.clone());

            // Check whether to stop refining
            if ni == 4 * deg {
                still_refining = false;
                //dbg!(rfn_count.clone());
            } else if ni >= 4 * deg {
                println!("Too many orbits created ({})", z_n.len());
                still_refining = false;
            }

            //dbg!(still_refining);

            // END REFINEMENT GENERATION
        } else {  // If not refining anymore
            // Reset z_temp
            z_temp = vec![];

            for (i, z)in z_n.iter().enumerate() {
                // Check if orbit has converged
                if pdp_n[i].abs() < eps_stop {
                    z_converged.push((*z, iteration));
                } else {
                    z_temp.push(z_n[i]);
                }
            }
            // ---UPDATE--- z_n
            z_n = z_temp;
        }
        //dbg!(z_n.len());
    }

    dbg!(still_refining);
    Ok(post_processing_roots(z_converged, eps_root, deg))
}

fn post_processing_roots(mut z_converged: Vec<(Complex<f64>, u32)>, eps_root: f64, deg: u32) -> Vec<(Complex<f64>, u32)> {
    // Vector for storing every determined root
    let mut roots: Vec<(Complex<f64>, u32)> = vec![];
    let mut previous;
    let mut temp_z_converged: Vec<(Complex<f64>, u32)> = vec![];
    while z_converged.len() != 0 && roots.len() as u32 != deg {
        // Set new determined root
        previous = z_converged[0].0;
        roots.push(z_converged[0]);

        for j in 0..z_converged.len() {
            // Check if other orbits are not too close to newly determined root
            if (previous - z_converged[j].0).abs() > eps_root {
                temp_z_converged.push(z_converged[j]);
            }
        }
        z_converged = temp_z_converged;
        temp_z_converged = vec![];
    }
    roots
}

fn get_t(z_n: &Vec<Complex<f64>>) -> Vec<Complex<f64>> {
    z_n.iter().enumerate().map(|(i, z)| -> Complex<f64> {
        // i-1 and i+1 while accounting for end points of the vector
        let l = z_n.len() - 1;
        let j = if i == 0 { l } else { i-1 };
        let k = if i == l { 0 } else { i+1 };

        // Calculate cross-ratio between three adjacent orbits
        (z_n[j] - *z) / (z_n[k] - *z)
    }).collect()
}

fn get_t_at_i(zi: Complex<f64>, z_prev: Complex<f64>, z_next: Complex<f64>) -> Complex<f64> {
    // Calculate cross-ratio between three adjacent orbits
    (z_prev - zi) / (z_next - zi)
}

fn get_rfn_condition(t_n: &Vec<Complex<f64>>, t_n0: &Vec<Complex<f64>>, rfn: f64) -> Vec<bool> {
    t_n.iter().enumerate().map(|(i, t_i)| {
        // a_n = |ln(t_n/t_n0)|
        (t_i/t_n0[i]).ln().abs() > rfn
    }).collect()
}

fn complex_vector_to_tuples(zs: &Vec<Complex<f64>>) -> Vec<(f64, f64)> {
    zs.iter().map(|z| (z.re(), z.im())).collect()
}