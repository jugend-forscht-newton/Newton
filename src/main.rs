pub mod newton;
pub mod polynomials;
pub mod data_output;
pub mod root_lists;

use newton::newton;
#[allow(unused)]
use data_output::newton_output;
use num::Complex;
#[allow(unused)]
use polynomials::{
    cheb_recursive, cheb_recursive_opt, cheb_normal, dcheb_normal, cheb_ruffini_eval,
    mdlbrt_hyp_cmp, dmdlbrt_hyp_cmp, mdlbrt_hyp_cmp_general, mdlbrt_hyp_cmp_opt,
    iterated_quadratic_opt,
    laguerre, dlaguerre, laguerre_general, laguerre_general_opt,
    legendre, dlegendre, legendre_general,
    hermite, dhermite, hermite_general, hermite_opt,
    wilkinson, dwilkinson, wilkinson_general,
    nroots, dnroots, nrooti, dnrooti,
    square_poly, dsquare_poly, square_grid_general,
    rand_on_disk_general,
};




fn main() {
    // For recursion depth
    let n = 8;

    // CHEBYSHEV
    //let deg = n;
    //let pdp = |z| { let (c, _, dc, _) = cheb_recursive(n, z); c / dc };
    //let pdp = |z| { let (c, _, dc, _) = cheb_recursive_opt(n, z); c / dc };
    //let pdp = |z| cheb_normal(z) / dcheb_normal(z);
    //let pdp = cheb_ruffini_eval;
    
    // MANDELBROT HYPERBOLIC COMPONENTS
    //let deg = (2 as u32).pow((n-1) as u32);
    //let pdp = |c| mdlbrt_hyp_cmp(n, c) / dmdlbrt_hyp_cmp(n, c);
    //let pdp = |c| { let (p, dp) = mdlbrt_hyp_cmp_general(n, c); p / dp };
    //let pdp = |c| mdlbrt_hyp_cmp_opt(n, c);

    // ITERATED QUADRATIC POLYNOMIAL
    let deg = (2 as u32).pow((n-1) as u32);
    let pdp = |z| iterated_quadratic_opt(n, z);
    
    // LAGUERRE
    //let deg = n;
    //let pdp = |z| laguerre(n, z) / dlaguerre(n, z);
    //let pdp = |z| { let (l, _, dl) = laguerre_general(n, z); l / dl };
    //let pdp = |z| { let (l, _, dl) = laguerre_general_opt(n, z); l / dl };

    // LEGENDRE
    //let deg = n;
    //let pdp = |z| legendre(n, z) / dlegendre(n, z);
    //let pdp = |z| { let (l, _, dl) = legendre_general(n, z); l / dl };

    // HERMITE
    //let deg = n;
    //let pdp = |z| hermite(n, z) / dhermite(n, z);
    //let pdp = |z| { let (h, dh) = hermite_general(n, z); h / dh };
    //let pdp = |z| hermite_opt(n, z);

    // For hermite160: choose radius 18. and definitely hermite_opt 

    // WILKINSON
    //let deg = n;
    //let pdp = |z| wilkinson(n, z + n as f64 / 2.) / dwilkinson(n, z + n as f64 / 2.);
    //let pdp = |z| { let (w, dw) = wilkinson_general(n, z + n as f64 / 2.); w / dw };

    // NROOTS / NROOTI
    //let deg = n;
    //let pdp = |z| nroots(n, z) / dnroots(n, z);
    //let pdp = |z| nrooti(n, z) / dnrooti(n, z);

    // ROOTS ON SQUARE GRID
    //let deg = (n+1)*(n+1);
    //let pdp = |z| square_poly(n as i32, z) / dsquare_poly(n as i32, z);
    //let deg = n;
    //let pdp = |z| { let (s, ds) = square_grid_general(n, z); s / ds };

    // RANDOM ROOTS ON A DISK
    //let deg = n;
    //let pdp = |z| { let (r, dr) = rand_on_disk_general(n, z); r / dr };

    // SHIFTING the function
    
    // LAGUERRE
    // Largest root of laguerre (approximately) used for shift
    //let largest_root = 630.;
    //let shift = largest_root / 2.;
    
    // WILKINSON
    //let largest_root = n+1;
    //let shift = largest_root as f64 / 8.;
    
    //let pdp = |z| pdp(z+shift);

    //dbg!(largest_root);
    //dbg!(shift);

    println!("Solving polynomial of degree {}", deg);
    let result = newton(
        2., 64, 4,
        1.0e-15, 1.0e-13,
        0.05, deg,
        &pdp
    );

    println!("Found {} roots", result.len());

    //let result = deflate_roots(result, shift);

    for z in result {
        println!("{:?},", (z.re, z.im));
    }
}


#[allow(unused)]
fn deflate_roots(zs: Vec<(Complex<f64>, u32)>, shift: f64) -> Vec<(Complex<f64>, u32)> {
    zs.iter().map(|(z, i)| (z + shift, *i)).collect()
}